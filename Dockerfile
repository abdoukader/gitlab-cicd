FROM openjdk:17-jdk-slim

LABEL maintainer="Abdou kader aktmere@gmail.com"

EXPOSE 8080

ADD target/gitlab-cicd.jar gitlab-cicd.jar

ENTRYPOINT ["java", "-jar", "gitlab-cicd.jar"]