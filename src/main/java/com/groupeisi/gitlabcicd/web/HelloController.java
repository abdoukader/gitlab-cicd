package com.groupeisi.gitlabcicd.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController {

    @GetMapping("/hellocicd")
    public ResponseEntity<String> message() {
        return new ResponseEntity<>("Salaam salaam Abdou kader...", HttpStatus.OK);
    }
}
